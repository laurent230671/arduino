#include <Wire.h>

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include "RTClib.h"

#define pinEnable    13 // Activation du driver/pilote
#define pinStep       9 // Signal de PAS (avancement)
#define pinDir        8 // Direction 

#define PIN_LED_R     6
#define PIN_LED_G     5
#define PIN_LED_B     3

#define PIN_BTN_1     2
#define PIN_BTN_2     4
#define PIN_BTN_3     7

#define capteur_lum   0  // capteur branché sur le port 0

#define OLED_RESET    4

// Ecran
Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

#ifndef GAMMA_H_
#define GAMMA_H_

/* Dependency for PROGMEM */
#include <avr/pgmspace.h>

/** Gamma correction table in flash memory. */
static const uint8_t PROGMEM _gamma[] = {
    0,  21,  28,  34,  39,  43,  46,  50,  53,  56,  59,  61,  64,  66,  68,  70,
   72,  74,  76,  78,  80,  82,  84,  85,  87,  89,  90,  92,  93,  95,  96,  98,
   99, 101, 102, 103, 105, 106, 107, 109, 110, 111, 112, 114, 115, 116, 117, 118,
  119, 120, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135,
  136, 137, 138, 139, 140, 141, 142, 143, 144, 144, 145, 146, 147, 148, 149, 150,
  151, 151, 152, 153, 154, 155, 156, 156, 157, 158, 159, 160, 160, 161, 162, 163,
  164, 164, 165, 166, 167, 167, 168, 169, 170, 170, 171, 172, 173, 173, 174, 175,
  175, 176, 177, 178, 178, 179, 180, 180, 181, 182, 182, 183, 184, 184, 185, 186,
  186, 187, 188, 188, 189, 190, 190, 191, 192, 192, 193, 194, 194, 195, 195, 196,
  197, 197, 198, 199, 199, 200, 200, 201, 202, 202, 203, 203, 204, 205, 205, 206,
  206, 207, 207, 208, 209, 209, 210, 210, 211, 212, 212, 213, 213, 214, 214, 215,
  215, 216, 217, 217, 218, 218, 219, 219, 220, 220, 221, 221, 222, 223, 223, 224,
  224, 225, 225, 226, 226, 227, 227, 228, 228, 229, 229, 230, 230, 231, 231, 232,
  232, 233, 233, 234, 234, 235, 235, 236, 236, 237, 237, 238, 238, 239, 239, 240,
  240, 241, 241, 242, 242, 243, 243, 244, 244, 245, 245, 246, 246, 247, 247, 248,
  248, 249, 249, 249, 250, 250, 251, 251, 252, 252, 253, 253, 254, 254, 255, 255
};

/**
 * Apply gamma correction to the given sample.
 *
 * @param x The input 8 bits sample value.
 * @return The output 8 bits sample value with gamma correction.
 */
static inline uint8_t gamma(uint8_t x) {
  return pgm_read_byte(&_gamma[x]);
}

#endif /* GAMMA_H_ */

// Horloge
RTC_DS3231 rtc;

// Valeur analogique envoyé par le capteur de luminosité
int analog_capteur_lum = 0;

int etat_bouton[3];
int dernier_etat_bouton[3];

unsigned long derniere_debounce[3];
unsigned long delai_debounce = 10;

boolean bOuverture = false;
boolean bFermeture = false;
boolean bAutomatique = false;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);  
  Serial.println("--- Start Serial Monitor ---");

    // I2C --------------------
    Wire.begin();
    // Real Time Clock
    rtc.begin();
    // ------------------------

    // MOTEUR -----------------
    pinMode( pinEnable, OUTPUT );
    pinMode( pinDir   , OUTPUT );
    pinMode( pinStep  , OUTPUT );
    // ------------------------    

    // ECRAN ------------------
    display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
    display.display(); 
    delay(2000);
    display.clearDisplay(); 
    display.display();
    // ------------------------    

    // BOUTONS ----------------
    pinMode( PIN_BTN_1, INPUT);
    pinMode( PIN_BTN_2, INPUT);
    pinMode( PIN_BTN_3, INPUT);

    for (int i=0;i<3;i++) {      
      dernier_etat_bouton[i] = HIGH;
      derniere_debounce[i] = 0;
    }    
    // ------------------------
}

void setDateTime() {
   rtc.adjust(DateTime(2019, 2, 9, 21, 42, 0));
}

/** Affiche une couleur */
void displayColor(byte r, byte g, byte b) {

  // Assigne l'état des broches
  // Version cathode commune
  analogWrite(PIN_LED_R, gamma(r & 255));
  analogWrite(PIN_LED_G, gamma(g & 255));
  analogWrite(PIN_LED_B, gamma(b & 255));
  // Version anode commune
  //analogWrite(PIN_LED_R, ~r);
  //analogWrite(PIN_LED_G, ~g);
  //analogWrite(PIN_LED_B, ~b);
}

boolean checkBouton(int btn, int pinBtn) {
  int etat = digitalRead(pinBtn); 
  if (etat != dernier_etat_bouton[btn]) {
     Serial.println("--> Changement");    
     unsigned long dt = millis() - derniere_debounce[btn];
     Serial.println(dt);
     if (dt > delai_debounce) {
        Serial.println("---> TEST ");
        derniere_debounce[btn] = millis();             
        etat_bouton[btn] = etat;
        Serial.print("Etat = [");
        Serial.print(etat);
        Serial.println("]");
        if (etat == LOW) {
          Serial.println("##OK##");
          return true;
        }
     }
  }
  return false;
}

void loop() {

  DateTime now = rtc.now();

  //Serial.print(now.hour());
  //Serial.print("  ");
  //Serial.println(now.minute());

  digitalWrite( pinDir   , HIGH); // Direction horaire
  digitalWrite( pinStep  , LOW);  // Initialisation de la broche step

  if (checkBouton(0,PIN_BTN_1)) {
     bOuverture = true;
     bFermeture = false;
  }
  if (checkBouton(1,PIN_BTN_2)) {
    bFermeture = true;
    bOuverture = false;
  }
  if (checkBouton(2,PIN_BTN_3)) {
    bAutomatique = not bAutomatique;
  }
  
  /*
  for(int i=0; i<50; i++){
    Serial.println( i );
    digitalWrite( pinStep, HIGH );
    delay( 1 );
    digitalWrite( pinStep, LOW );
    delay( 1 );
  } 
*/
  // Luminosité
  analog_capteur_lum = analogRead(capteur_lum);
    
  display.clearDisplay(); 
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.print(now.hour());
  display.print("  ");
  display.print(now.minute());  
  display.print("  ");
  display.println(now.second());    
  display.print("Lum : [");
  display.print(analog_capteur_lum);
  display.println("]");
  display.print("Ouverture = [");
  display.print((bOuverture ? "OK" : "NON"));
  display.println("]");
  display.print("Fermeture = [");
  display.print((bFermeture ? "OK" : "NON"));
  display.println("]");
  display.print("Automatique = [");
  display.print((bAutomatique ? "AUTO" : "MANUEL"));
  display.println("]");

  display.display(); 
  delay(1);
/*
  displayColor(255, 0, 0);
  delay(2000);
  displayColor(0, 255, 0);
  delay(2000);
  displayColor(204, 102, 0);
  delay(2000);

*/
}
