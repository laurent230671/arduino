//#include "images.h"
//#include "BH1750.h"

// UV
//#include <GUVA-S12SD.h>
// Baromètre
#include "Adafruit_BMP085.h"
// T° & Hum. HP
#include "Adafruit_Si7021.h"
// Ecran 
#include "heltec.h" 

#define PIN_UV     39  // GUVA-S12SD --- UV
#define PIN_RAIN   36  // Capteur pluie

#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  28       /* Time ESP32 will go to sleep (in seconds) */
RTC_DATA_ATTR long counter = 0;

#define BAND    868E6  //you can set band here directly,e.g. 868E6,915E6

String rssi = "RSSI --";
String packSize = "--";
String packet ;

// --------------------------------------------------------------
// --------------------------------------------------------------
// Si7021 --- T° & Humidité
Adafruit_Si7021 sensor = Adafruit_Si7021();
float temp = 0;
float humidity = 0;
// ------------------------
// BMP180 --- Baromètre
Adafruit_BMP085 bmp;
long pressure = 0;
// ------------------------
// Capteur pluie ----------
int rainValue = 0;
//bool rain = false;
//int thresholdValue = 2200;
// ------------------------
// Capteur UV -------------
//int GUVA_PIN = 39;
//float GUVA_VOLTAGE = 3.3;
//int SAMPLING_COUNT = 100;
//GUVAS12SD uv(GUVA_PIN, GUVA_VOLTAGE, SAMPLING_COUNT);
//float mV = 0;
//float uv_index = 0;

void setup()
{
  //Serial.begin(115200);
   
   //WIFI Kit series V1 not support Vext control
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.Heltec.Heltec.LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
 
  Heltec.display->init();
  Heltec.display->flipScreenVertically();  
  Heltec.display->setFont(ArialMT_Plain_10);
  Heltec.display->clear();
  
  Heltec.display->drawString(0, 0, "Heltec.LoRa Initial success!");
  Heltec.display->display();
  delay(1000);
  
  //Initialize the I2C sensors and ping them
  if (!sensor.begin()) {
    Heltec.display->clear();
    Heltec.display->drawString(0, 0, "Did not find Si7021 sensor!");
    Heltec.display->display();
    delay(1000);
    while (true);
  }

  if (!bmp.begin()) {
    Heltec.display->clear();
    Heltec.display->drawString(0, 0, "Could not find a valid BMP085 sensor, check wiring!");
    Heltec.display->display();
    delay(1000);
    while (true);
  }
  
  Heltec.display->displayOff(); // <- Pas besoin de consommer du courant pour rien :)
  digitalWrite(LED, LOW);
}

String floatEncoder(String parameterName, float parameterValue) {
   String pName;
   String pValue;
   pName = "F" + (parameterName + "     ").substring(0, 5);
   pValue = "000000" + String(parameterValue, 2);
   return  pName + pValue.substring(pValue.length()-6);
}

String longEncoder(String parameterName, long parameterValue) {
   String pName;
   String pValue;
   pName = "L" + (parameterName + "     ").substring(0, 5);
   pValue = "000000" + String(parameterValue, DEC);
   return  pName + pValue.substring(pValue.length()-6);
}

String boolEncoder(String parameterName, long parameterValue) {
   String pName;
   String pValue;
   pName = "B" + (parameterName + "     ").substring(0, 5);
   pValue = (parameterValue ? "000001" : "000000");
   return  pName + pValue;
}

//---------------------------------------------------------------------
// MAIN LOOP
//---------------------------------------------------------------------
void loop()
{
  temp = sensor.readTemperature();
  humidity = sensor.readHumidity();

  //float sum = 0;
  //for (int i=0; i<1000; i++) {
  //  float v = analogRead(GUVA_PIN);
  //  sum = v + sum;
  //  delay(10);
  //}
  //mV = (sum / 100);
  // uv_index = (mV * GUVA_VOLTAGE / 1024.0) * 10;
  
  pressure = bmp.readPressure();

  rainValue = analogRead(PIN_RAIN);
  //rain = (rainValue < thresholdValue);

  /* 
  Heltec.display->clear();
  Heltec.display->setTextAlignment(TEXT_ALIGN_LEFT);
  Heltec.display->setFont(ArialMT_Plain_10);
  
  //Heltec.display->drawString(0, 0, "Sending packet: ");
  //Heltec.display->drawString(85, 0, String(counter));

  Heltec.display->drawString(0, 10, "Temperature :");
  Heltec.display->drawString(85, 10, String(temp)+ " c");
  
  Heltec.display->drawString(0, 20, "Humidite :");
  Heltec.display->drawString(85, 20, String(humidity)+ " %");

  Heltec.display->drawString(0, 30, "mV :");
  Heltec.display->drawString(85, 30, String(mV));

  Heltec.display->drawString(0, 40, "Index :");
  Heltec.display->drawString(85, 40, String(uv_index));


  Heltec.display->drawString(0, 50, "P.A. :");
  Heltec.display->drawString(55, 50, String(pressure)+ " Pa");
  */

  //if (rain) {
  //    Heltec.display->drawString(0, 50, "Il pleut ! ");  
  //} else {
  //  Heltec.display->drawString(0, 50, "Temps sec ! ");
  //}

  //Heltec.display->display();

  // send packet
  LoRa.beginPacket();
  
/*
 * LoRa.setTxPower(txPower,RFOUT_pin);
 * txPower -- 0 ~ 20
 * RFOUT_pin could be RF_PACONFIG_PASELECT_PABOOST or RF_PACONFIG_PASELECT_RFO
 *   - RF_PACONFIG_PASELECT_PABOOST -- LoRa single output via PABOOST, maximum output 20dBm
 *   - RF_PACONFIG_PASELECT_RFO     -- LoRa single output via RFO_HF / RFO_LF, maximum output 14dBm
*/
  LoRa.setTxPower(14,RF_PACONFIG_PASELECT_PABOOST);

  packet = floatEncoder("T_OUT",temp) + floatEncoder("H_OUT",humidity) + longEncoder("PA",pressure) + longEncoder("RAIN",rainValue) + longEncoder("COUNT",counter); // + floatEncoder("UV",mV); longEncoder("LUX",lux) +
  LoRa.print(packet);
  LoRa.endPacket();

  counter++;
  //digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)
  //delay(1000);               // wait for a second
  //digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW
  //delay(1000);               // wait for a second
  
  if (counter > 999999) {
    counter = 0;
  }

  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  esp_deep_sleep_start();
  //delay(20000);  // +/- 3 set de valeurs par minute.
  
}
