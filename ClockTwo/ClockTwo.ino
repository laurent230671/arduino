#define FASTLED_INTERRUPT_RETRY_COUNT 0
#define FASTLED_ESP8266_RAW_PIN_ORDER

#define NUM_LEDS   114
#define LEDS_PIN   2

#define BRIGHTNESS_SENSOR  0
#define BRIGHTNESS_MIN  40 
#define BRIGHTNESS_MAX  1500

#define BH1750_BRIGHTNESS_MIN 1
#define BH1750_BRIGHTNESS_MAX 65535

#define HEURE_HIVER 3600
#define HEURE_ETE   7200

#define UDP_REFRESH 1800000 // 30 minutes

#define ONBOARDLED 2 // Built in LED on ESP-12/ESP-07

#define COLOR1_CHRISTMAS 0xff0000 // RED
#define COLOR2_CHRISTMAS 0xffffff // WHITE

#define COLOR1_NEWYEAR   0x0000ff // BLUE
#define COLOR2_NEWYEAR   0x00ffff // BLUE

#include "TimeLib.h"
#include <Wire.h>
#include <WiFiUdp.h>
#include <ESP8266WiFi.h>
#include <NTPClient.h>
#include "RTClib.h"
#include <FastLED.h>
#include <BH1750.h>
#include <time.h>

const char* ssid = "decoWifi";
const char* password = "LoLa2013";

int _display[27][6] = {{ 0, 1, -1, -1, -1, -1}, // 0  IL
  { 3, 4, 5, -1, -1, -1}, // 1  EST
  { 7, 8, 9, 10, -1, -1}, // 2  DEUX
  {16, 17, 18, 19, 20, 21}, // 3  QUATRE
  {11, 12, 13, 14, 15, -1}, // 4  TROIS
  {22, 23, 24, 25, -1, -1}, // 5  NEUF
  {26, 27, 28, -1, -1, -1}, // 6  UNE
  {29, 30, 31, 32, -1, -1}, // 7  SEPT
  {40, 41, 42, 43, -1, -1}, // 8  HUIT
  {37, 38, 39, -1, -1, -1}, // 9  SIX
  {33, 34, 35, 36, -1, -1}, // 10 CINQ
  {44, 45, 46, 47, -1, -1}, // 11 MIDI
  {46, 47, 48, -1, -1, -1}, // 12 DIX
  {49, 50, 51, 52, 53, 54}, // 13 MINUIT
  {62, 63, 64, 65, -1, -1}, // 14 ONZE
  {56, 57, 58, 59, 60, -1}, // 15 HEURE
  {55, -1, -1, -1, -1, -1}, // 16 S
  {66, 67, 68, 69, 70, -1}, // 17 MOINS
  {72, 73, -1, -1, -1, -1}, // 18 LE
  {74, 75, 76, -1, -1, -1}, // 19 DIX
  {86, 87, -1, -1, -1, -1}, // 20 ET
  {80, 81, 82, 83, 84, -1}, // 21 QUART
  {88, 89, 90, 91, 92, -1}, // 22 VINGT
  {93, -1, -1, -1, -1, -1}, // 23 -
  {94, 95, 96, 97, -1, -1}, // 24 CINQ
  {108, 109, -1, -1, -1, -1}, // 25 ET
  {102, 103, 104, 105, 106, -1} // 26 DEMIE
};

int _decompte[] = { 113, 112, 111, 110 } ; // { 110, 111, 112, 113 }

int _hours[] = {6,  // 0  UNE
                2,  // 1  DEUX
                4,  // 2  TROIS
                3,  // 3  QUATRE
                10, // 4  CINQ
                9,  // 5  SIX
                7,  // 6  SEPT
                8,  // 7  HUIT
                5,  // 8  NEUF
                12, // 9  DIX
                14, // 10 ONZE
                11, // 11 MIDI
                13  // 12 MINUIT
               };

int _minutes[] = { 24, -1, -1, -1,  //  0 ->  3 - CINQ
                   19, -1, -1, -1,  //  4 ->  7 - DIX
                   20, 21, -1, -1,  //  8 -> 11 - ET QUART
                   22, -1, -1, -1,  // 12 -> 15 - VINGT
                   22, 23, 24, -1,  // 16 -> 19 - VINGT-CINQ
                   25, 26, -1, -1,  // 20 -> 23 - ET DEMIE
                   17, 22, 23, 24,  // 24 -> 27 - MOINS VINGT-CINQ
                   17, 22, -1, -1,  // 28 -> 31 - MOINS VINGT
                   17, 18, 21, -1,  // 32 -> 35 - MOINS LE QUART
                   17, 19, -1, -1,  // 36 -> 39 - MOINS DIX
                   17, 24, -1, -1   // 40 -> 43 - MOINS CINQ
                 };

int _current_day = 0;
                 
String _color = "d16a0e";
CRGB _leds[NUM_LEDS];
CRGB _ledColor = 0xd16a0e;
CRGB _ledColor2 = 0xd16a0e;

bool _wifiConnected = false;

WiFiClient _espClient;
WiFiServer _server(80);
//WiFiUDP _ntpUDP;

// RTC DS3231
RTC_DS3231 _rtClock;
DateTime _rtClockTime;
unsigned long _updateUDP = 0;

// BH 1750 BRIGHTNESS
uint16_t _sensorMin = BH1750_BRIGHTNESS_MIN;
uint16_t _sensorMax = BH1750_BRIGHTNESS_MAX;
uint16_t _lux = 0;
int _brightnessValue = 0;
int _prevBrightnessValue = -1;

BH1750 _lightMeter(0x23);

int _previous_hours = -1;
int _previous_minutes = -1;
int _previous_decompte = 0;

void setLeds(int displayElt, boolean state) {
  DateTime dt = _rtClockTime;
  setLeds(displayElt, state, _ledColor, _ledColor2);
}

void setLeds(int displayElt, boolean state, CRGB color, CRGB color2) {
  CRGB currentColor = color;
  int l = 0;
  if (displayElt >= 0 && displayElt <= 27) {
    for (int i = 0; i < 6; i++) {
      if (_display[displayElt][i] != -1) {
        if (state) {
          l++;
          if (l % 2 == 0) {
              currentColor = color2;
          } else {
              currentColor = color;
          }       
        } else {
          currentColor = CRGB::Black;
        }
        _leds[_display[displayElt][i]] = currentColor;
      }
    }
  }
}

void display_IL_EST() {  
  CRGB color = (_wifiConnected ? _ledColor : CRGB::Red);
  CRGB color2 = (_wifiConnected ? _ledColor2 : CRGB::Red);
  setLeds(0, true, color, color2); // IL  
  setLeds(1, true, color, color2); // EST
}

void setDecompteOff() {
  _leds[_decompte[0]] =  CRGB::Black;
  _leds[_decompte[1]] =  CRGB::Black;
  _leds[_decompte[2]] =  CRGB::Black;
  _leds[_decompte[3]] =  CRGB::Black;  
  _previous_decompte = 0;
}

void testLeds() {
  int x;
  CRGB testcolor = CRGB(255,255,51);
  FastLED.setBrightness(100);
  for (x=0;x<NUM_LEDS;x++) {
      _leds[x] = testcolor;
      FastLED.show();
      delay(25);
      _leds[x] = CRGB::Black;
      delay(5);
  }
  FastLED.show();
}

//===============================================================================================
// SETUP
//===============================================================================================
void setup() {
  // int status = WL_IDLE_STATUS;
  static WiFiEventHandler e1, e2, e3;

  pinMode (ONBOARDLED, OUTPUT); // Onboard LED
  digitalWrite (ONBOARDLED, HIGH); // Switch off LED

  Serial.begin(115200);
  Serial.println("");
  Serial.print("Startup reason : ");
  Serial.println(ESP.getResetReason());

  /*
  IPAddress ip(192,168,1,80);   
  
  IPAddress gateway(192,168,1,1);   
  IPAddress subnet(255,255,255,0);   
  IPAddress dns1(192,252,172,57);
  IPAddress dns2(149,154,159,92);
  
  if (!WiFi.config(ip, gateway, subnet, dns1, dns2)) {
    Serial.println("*************************************************************************");
    Serial.println("WIFI CONFIG RETURN FALSE !!!!");
    Serial.println("*************************************************************************");
    while(1){};
  }
  */
  
  WiFi.mode (WIFI_STA);
  WiFi.begin(ssid, password);
  
  FastLED.addLeds<WS2812B, LEDS_PIN, GRB>(_leds, NUM_LEDS);

  // I2C SETUP --------------------
  Wire.begin();
  // Real Time Clock
  _rtClock.begin();
  // Light detector
  _lightMeter.begin();  
  // -------------------------- I2C
 
  e1 = WiFi.onStationModeGotIP (onSTAGotIP);
  e2 = WiFi.onStationModeDisconnected (onSTADisconnected);
  e3 = WiFi.onStationModeConnected (onSTAConnected);
  
  testLeds();
  // display_IL_EST();  
}

void onSTAConnected (WiFiEventStationModeConnected ipInfo) {
    Serial.println("------------------------------------------------------");
    Serial.printf ("Connected to %s\r\n", ipInfo.ssid.c_str ());
    Serial.println("------------------------------------------------------");
}


// Start NTP only after IP network is connected
void onSTAGotIP (WiFiEventStationModeGotIP ipInfo) {
    Serial.printf ("Got IP: %s\r\n", ipInfo.ip.toString ().c_str ());
    Serial.printf ("Connected: %s\r\n", WiFi.status () == WL_CONNECTED ? "yes" : "no");
    // Démarrage du client NTP - Start NTP client
    _server.begin();
    _wifiConnected = true; 
    display_IL_EST();   
}

// Manage network disconnection
void onSTADisconnected (WiFiEventStationModeDisconnected event_info) {
    Serial.printf ("Disconnected from SSID: %s\n", event_info.ssid.c_str ());
    Serial.printf ("Reason: %d\n", event_info.reason);
    _server.stop();
    _wifiConnected = false;
    display_IL_EST();
}

//===============================================================================================
// Check UDP for Synchronization time
//===============================================================================================
const long getHeureEteHiver(int j, int m, int a) {  
  Serial.print("m="); Serial.print(m); Serial.println();
  if (m==3 || m == 10) {
    // Méthode dérivée de celle de Gauss, valable pour toutes les 
    // dates allant de +1752 à +8000
    // Ce qui nous interesse est de trouver le jour du 31/03 ou du 31/10.
    // ex : pour le mois d'octobre 2020 => le jour = 6 ( càd samedi puisque 0=dimanche )
    //      Donc le dernier dimanche du mois d'octobre 2020 sera => 31-6 => 25 
     float mc = m-2; 
     float s = int(a/100);
     a -= s * 100;     
     float b = int(2.6 * mc - .19) + 31 + a + int(a/4) + int(s/4) - s * 2;
     float bt = b/7;
     int ddwe = 31 - int((bt - int(bt)) * 7 + .1);

     if (m == 3) {
        if (j < ddwe) {
          return HEURE_HIVER;
        } else {
          return HEURE_ETE;
        }
     } else {
        if (j < ddwe) {
          return HEURE_ETE;
        } else {
          return HEURE_HIVER;
        }      
     }    
  } else {      
    if (m > 3 && m < 10) {
        return HEURE_ETE;
    } else {
        return HEURE_HIVER;
    }
  }  
}
void TimeSynchronization() {
    WiFiUDP ntpUDP;
    DateTime dt = _rtClockTime;
    long utcOffsetInSeconds = getHeureEteHiver(dt.day(),dt.month(),dt.year());
    
    // UDP
    NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", utcOffsetInSeconds);
    timeClient.begin();
    if (timeClient.update()) {
       time_t utcCalc = timeClient.getEpochTime();
       DateTime dt = _rtClockTime;
       
       ConsoleDisplayTime(dt, utcCalc, timeClient);
          
       if ((timeClient.getHours() != dt.hour()) || 
           (timeClient.getMinutes() != dt.minute()) || 
           (timeClient.getSeconds() != dt.second()) ||
           (day(utcCalc) != dt.day()) || 
           (month(utcCalc) != dt.month()) || 
           (year(utcCalc) != dt.year())) {
              _rtClock.adjust(DateTime(year(utcCalc), month(utcCalc), day(utcCalc), timeClient.getHours(), timeClient.getMinutes(), timeClient.getSeconds()));              
              Serial.printf ("### TIME UPDATED ###\n");
       }    
    }
    timeClient.end();
}

void ConsoleDisplayTime(DateTime dt, time_t utcCalc, NTPClient timeClient) {
   Serial.printf ("Now is %02d/%02d/%04d %02d:%02d:%02d\r\n", 
                  dt.day(),
                  dt.month(),
                  dt.year(),
                  dt.hour(),
                  dt.minute(),
                  dt.second());
   Serial.printf ("UTC is %02d/%02d/%04d %02d:%02d:%02d\r\n", 
                  day(utcCalc),
                  month(utcCalc),
                  year(utcCalc),
                  timeClient.getHours(),
                  timeClient.getMinutes(),
                  timeClient.getSeconds());                  
}

void getCurrentDateTime() {
    // Lecture de l'heure en cours sur DS3231
    _rtClockTime = _rtClock.now(); 
    SetColor(_rtClockTime.day());
    if (_wifiConnected && _updateUDP < millis()) {      
      _updateUDP = millis() + UDP_REFRESH;
      Serial.println("!!! CheckTIME !!!");
      TimeSynchronization();
    } 
}

int HexToDec(String hex) {
    int i;
    int result = 0;
    for (i = 0; i <hex.length(); i++) {
      if (hex[i]>=48 && hex[i]<=57) {
        result += (hex[i]-48)*pow(16,hex.length()-i-1);
      } else if (hex[i]>=65 && hex[i]<=70) {
        result += (hex[i]-55)*pow(16,hex.length( )-i-1);
      } else if (hex[i]>=97 && hex[i]<=102) {
        result += (hex[i]-87)*pow(16,hex.length()-i-1);
      }       
    }
    return result;
}

void ProcessNewClient(WiFiClient client) {
  // Variable to store the HTTP request
  String header;

  Serial.println("");
  Serial.println("New Client :"); // print a message out in the serial port
  String currentLine = ""; // make a String to hold incoming data
  int cR = 0;
  int cG = 0;
  int cB = 0;
  unsigned long timeout = millis();

  
  while (client.connected()) { // loop while the client's connected
    
    if (millis() - timeout > 2000) {
      Serial.println("TIME OUT !");
      break;
    }    

    if (client.available()) { // if there's bytes to read      
      
      char c = client.read(); // read a byte, then
      Serial.write(c); // print it out the serial monitor
      header += c;
      if (c == '\n') { // if the byte is a newline character
                       // if the current line is blank, you got two newline characters in a row.
                       // that's the end of the client HTTP request, so send a response:
        if (currentLine.length() == 0) {
                      // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
                      // and content-type so the client knows what's coming, then a blank line:
          client.println("HTTP/1.1 200 OK");
          client.println("Content-type:text/html");
          client.println("Connection: close");
          client.println(); 

          int i = 0;
          i = header.indexOf("GET /?head=%23");
          if (i >= 0) {          
             _color = header.substring(i+14,i+20);              
             cR = HexToDec(header.substring(i+14,i+16).c_str());
             cG = HexToDec(header.substring(i+16,i+18).c_str());
             cB = HexToDec(header.substring(i+18,i+20).c_str());
             Serial.printf ("R=[%d] G=[%d] B=[%d]\r\n",cR,cG,cB);
             _ledColor = CRGB(cR,cG,cB);             
             _previous_hours = -1;
             _previous_minutes = -1;
             _previous_decompte = 0;
             display_IL_EST();
          }     

          i = header.indexOf("GET /?head=Reset");
          if (i >= 0) {          
            Serial.println("RESET");
          }

                                   
          // Display the HTML web page
          client.println("<!DOCTYPE html><html>");
          client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
          client.println("<link rel=\"icon\" href=\"data:,\">"); 
          // CSS to style the on/off buttons
          // Feel free to change the background-color and font-size attributes
          client.println("<style>html { font-family: Helvetica; display:inline-block; margin: 0px auto; text-align: center;}");
          client.println(".button { background-color: #195B6A; border:none; color: white; padding: 16px 40px;");
          client.println("text-decoration: none; font-size: 30px; margin:2px; cursor: pointer;}");
          client.println(".button2 {background-color:#77878A;}</style></head>");        
          // Web Page Heading
          client.println("<body><h1>ClockTwo Lolo</h1>");
          client.println("<form>");                   
          client.print("<div><input type=\"color\" id=\"head\" name=\"head\" value=\"#"+_color+"\"></div>"); 
          client.println("<p> <input type=\"submit\" value=\"Changer la couleur\"></p>");                   
          client.println("</form></body></html>");
          // The HTTP response ends with another blank line
          client.println();
          // Break out of the while loop
          break;
        } else { // if you got a newline, then clear currentLine
          currentLine = "";
        }
      } else if (c != '\r') { // if you got anything else but a carriage return character,
        currentLine += c; // add it to the end of the currentLine
      }    
    }
  }
}

void updateClock(int h, int m) {
  // Serial.printf("updateClock(%d,%d) BrightnessValue = %d\r\n",h,m, _brightnessValue);  
  FastLED.setBrightness(_brightnessValue);

  int decompte = m % 5;
  int minutes = m - decompte;
  int hours = ((minutes > 30) ? (h + 1) : h);

  // Serial.printf("=> hours = %d - Minutes = %d\r\n", hours, minutes);

  if (hours == 0 || hours == 24) {
    hours = 13; // MINUIT
  } else {   
    if (hours > 12) {
      hours = hours - 12;
    }    
  }

  // HEURES
  if (_previous_hours != hours) {
    Serial.print("On change les heures ==> ");
    Serial.println(hours);
    setHours(_previous_hours, false);
    _previous_hours = hours;
    setHours(hours, true);
  }

  // MINUTES
  if (_previous_minutes != minutes) {
    Serial.print("On change les minutes ==> ");
    Serial.println(minutes);
    setMinutes(_previous_minutes, false);    
    _previous_minutes = minutes;        
    setMinutes(minutes, true);
    setDecompteOff();
  } 
  
  for (int d=0;d<decompte;d++) {  
     _leds[_decompte[d]] = _ledColor;      
  }  
  
  delay(150);
  FastLED.show();
  
}

void setHours(int hours, boolean state) {
  if ((hours >= 1) && (hours <= 13)) { // 13 = MINUIT
    boolean midiMinuit = ((_hours[hours - 1] == 11) || (_hours[hours - 1] == 13));    
    setLeds(_hours[hours - 1], state);
    setLeds(15, !midiMinuit); // On retire ou affiche l'HEURE   
    if ((!midiMinuit) && (hours > 1)) {
      setLeds(16, state); // heureS <= pluriel !
    }    
  }
}

void setMinutes(int minutes, boolean state) {
  if ((minutes >= 0) && (minutes <= 60)) {
    // les minutes sont obligatoirement des multiples de 5.
    int m = (minutes / 5) - 1;
    if ( m >= 0) {
      for (int i = 0; i < 4; i++) {
        if (_minutes[(m * 4) + i] != -1) {          
          setLeds(_minutes[(m * 4) + i], state);          
        }
      }
    }   
  }
}

void SetColor(int today) {
    if (today != _current_day) {
        _current_day = today;
        DateTime dt = _rtClockTime;                

        // default
        _ledColor = 0xd16a0e;
        _ledColor2 = 0xd16a0e;
        
        if (dt.day() == 25 && dt.month() == 12) { 
          _ledColor = COLOR1_CHRISTMAS;
          _ledColor2 = COLOR2_CHRISTMAS; 
        } else {
           if (dt.day() == 1 && dt.month() == 1) {
              _ledColor = COLOR1_NEWYEAR;
              _ledColor2 = COLOR2_NEWYEAR;
           }
        }
        display_IL_EST();
    }
}

//===============================================================================================
// LOOP
//===============================================================================================
void loop() {
    if (_wifiConnected) {          
          WiFiClient client = _server.available(); // Listen for incoming clients   
          if (client) { // If a new client connects
              //process client
              ProcessNewClient(client);
              // Close the connection
              client.stop();              
              Serial.println("Client disconnected.");
              Serial.println("");
          }
    } 
    _lux = _lightMeter.readLightLevel();    
    //Serial.print("Lux = [");
    //Serial.print(_lux);
    //Serial.println("]");
    _lux += (_lux * .30);
    _brightnessValue = map(_lux, _sensorMin, _sensorMax, BRIGHTNESS_MIN, BRIGHTNESS_MAX);
    // _brightnessValue += (_brightnessValue * .30 );
    // _brightnessValue = constrain(_brightnessValue, BRIGHTNESS_MIN, BRIGHTNESS_MAX);
    //Serial.print("brightnessValue = [");
    //Serial.print(_brightnessValue);
    //Serial.println("]");
      
    getCurrentDateTime();    
    updateClock(_rtClockTime.hour(), _rtClockTime.minute()); 

    delay(300);
}
