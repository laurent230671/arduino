#include "heltec.h" 
#include "WiFi.h"
#include "Adafruit_Si7021.h"
#include "newFont.h"


#define BAND 868E6  //you can set band here directly,e.g. 433E6,868E6,915E6

String packSize = "--";
String packet ;

float NULL_VALUE = -999.9;
int MAX_RETRY = 600; 
           
int CHANGE_STATE = 10;

// Si7021 --- T° & Humidité
Adafruit_Si7021 sensor = Adafruit_Si7021();
float temp = NULL_VALUE;
float humidity = NULL_VALUE;
// ------------------------

float temp_out = NULL_VALUE;
float humidity_out = NULL_VALUE;
float mV = -1;
int pa = 0;

const char* host = "192.168.1.175";
const uint16_t port = 1700;

// ------------------------
// ------------------------
int retry = 0;
int state = 0;
int stateLoop = CHANGE_STATE;

int width = 128; // largeur de l'écran
int height = 64; // hauteur de l'écran
int center = 64; // center de l'écran en largeur = 128 / 2
int margin = 6;  // marge
int unit_l= 47;  // décalage pour l'affichage des unités en largeur 
int unit_t= 17;  // position pour affichage des unités température
int unit_h= 42;  // position pour affichage des unités humiditée
int line0 = 0;   // Position ligne 1
int line1 = 19;  // Position ligne 2
int line2 = 40;  // Position ligne 3

WiFiClient client;

String floatEncoder(String parameterName, float parameterValue) {
   String pName;
   String pValue;
   pName = "F" + (parameterName + "     ").substring(0, 5);
   pValue = "000000" + String(parameterValue, 2);
   return  pName + pValue.substring(pValue.length()-6);
}

String longEncoder(String parameterName, long parameterValue) {
   String pName;
   String pValue;
   pName = "L" + (parameterName + "     ").substring(0, 5);
   pValue = "000000" + String(parameterValue, DEC);
   return  pName + pValue.substring(pValue.length()-6);
}

String boolEncoder(String parameterName, long parameterValue) {
   String pName;
   String pValue;
   pName = "B" + (parameterName + "     ").substring(0, 5);
   pValue = (parameterValue ? "000001" : "000000");
   return  pName + pValue;
}


void displayTemp(float t, String title) {
    Heltec.display->clear();
    // Titre ==> ARIAL 16
    Heltec.display->setTextAlignment(TEXT_ALIGN_LEFT);
    Heltec.display->setFont(ArialMT_Plain_16);
    Heltec.display->drawString(0,line0, title);     
    // t° ==> 
    Heltec.display->setTextAlignment(TEXT_ALIGN_RIGHT);
    Heltec.display->setFont(Roboto_45);
    if (t < -100.0) {
      Heltec.display->drawString(width-margin-5,line1-5, "---");      
    } else {    
      Heltec.display->drawString(width-margin-5,line1-5, String(t,1));
    }
    // Unités ==> ARIAL 10
    Heltec.display->setFont(ArialMT_Plain_10);   
    Heltec.display->drawString(width-margin,unit_t, "o");        
    Heltec.display->display();
}

void displayUV(float mV) {
  String str;
  int uvIdx;
  Heltec.display->clear();
  Heltec.display->setTextAlignment(TEXT_ALIGN_CENTER);
  Heltec.display->setFont(ArialMT_Plain_16);
  Heltec.display->drawString(center,line0, "Index U.V.");    
  uvIdx = (mV * 3.3 / 1024.0); //*10 pas de multiplication par 10 pour corriger l'erreur du sender ( boucle de 1000 mais division par 100 !)
  if (uvIdx < 0 || uvIdx > 20) {
      uvIdx = -1;
      str = "[?]";
  } else {
      str = "[" + String(uvIdx, DEC) + "]";
  }
  Heltec.display->drawString(center,line1, str);       
  switch (uvIdx) {    
    case -1: str = "-----"; break;
    case 0:
    case 1:
    case 2: str = " BAS "; break;
    case 3:
    case 4:
    case 5: str = "- MODERE -"; break;
    case 6:
    case 7: str = "+ HAUT +"; break;
    case 8:
    case 9:
    case 10: str= "++ TRES HAUT ++"; break;
    default: str=" ** EXTREME ** "; break;
  }
  Heltec.display->drawString(center,line2, str);   
  Heltec.display->display();  
}

void displayPA(int pPa) {
  Heltec.display->clear();
  Heltec.display->setTextAlignment(TEXT_ALIGN_CENTER);
  Heltec.display->setFont(ArialMT_Plain_16);
  String str;
  float fpa = (float)pPa / 100;
  if (pPa < 0) {
    fpa = 0;  
  }
  str = String(fpa,2) + " hPa";
  Heltec.display->drawString(center,line0, "Pression");   
  Heltec.display->drawString(center,line1, "atmosphérique");   
  Heltec.display->drawString(center,line2, str);   
  Heltec.display->display();  
}

void displayData(float tIn, float hIn, float tOut, float hOut) {

  Heltec.display->clear();
  // Titre ==> ARIAL 16
  Heltec.display->setTextAlignment(TEXT_ALIGN_CENTER);
  Heltec.display->setFont(ArialMT_Plain_16);
  Heltec.display->drawString(center / 2,line0, "Int");  
  Heltec.display->drawString(center + (center / 2),line0, "Ext");  

  // t° et H in et Out ==> ARIAL 24
  Heltec.display->setTextAlignment(TEXT_ALIGN_LEFT);
  Heltec.display->setFont(ArialMT_Plain_24);
  Heltec.display->drawString(margin,line1, String(tIn,1));  
  Heltec.display->drawString(margin,line2, String(hIn,1));  
  if (tOut < -100.0) {
    Heltec.display->drawString(center + margin,line1, "---");      
  } else {    
    Heltec.display->drawString(center + margin,line1, String(tOut,1));      
  }
  if (hOut < -100.0) {
    Heltec.display->drawString(center + margin,line2, "---");  
  } else {
    Heltec.display->drawString(center + margin,line2, String(hOut,1));    
  }

  // Unités ==> ARIAL 10
  Heltec.display->setFont(ArialMT_Plain_10);
  Heltec.display->drawString(unit_l + margin,unit_h, "%");  
  Heltec.display->drawString(center + unit_l + margin,unit_h, "%");  
  Heltec.display->drawString(unit_l + margin,unit_t, "o");   
  if (tOut < 0) {
    Heltec.display->drawString(center + unit_l + margin+5,unit_t, "o");  
  } else {
    Heltec.display->drawString(center + unit_l + margin,unit_t, "o");    
  }

  // Ligne de séparation
  Heltec.display->drawLine(center,0,center,height);
  Heltec.display->display();  
}

void LoRaData(int packetSize){
  // Remise à zéro du compteur retry (Utilisé pour savoir si l'on reçoit bien les données de l'émetteur)
  retry = 0;

  // Lecture de la température et humidité dans la maison
  temp = sensor.readTemperature();
  humidity = sensor.readHumidity();

  // On ajoute ces 2 valeurs (t° et H) à la chaine que l'on a reçu de l'émetteur
  packet = packet + floatEncoder("T_IN",temp) + floatEncoder("H_IN",humidity);

  // On va récuperer quelques valeurs reçu de l'émetteur 
  // pour les afficher sur l'écran
  String tmp;
  for (int i=0; i<packetSize; i+=12) {
      tmp = packet.substring(i,i+12);
      if (tmp.substring(1,6) == "T_OUT") {
        temp_out = tmp.substring(6,12).toFloat();
      }
      if (tmp.substring(1,6) == "H_OUT") {
        humidity_out = tmp.substring(6,12).toFloat();
      }
      if (tmp.substring(1,3) == "UV") {
        mV = tmp.substring(6,12).toFloat();
      }
      if (tmp.substring(1,3) == "PA") {
        pa = tmp.substring(6,12).toInt();
      }
  }

  // affichage des informations sur l'écran
  // displayData(temp, humidity, temp_out, humidity_out);
  
  // Envoi des informations au serveur avec la base de données
  int status = client.connect(host,port);
  if (status != 0) {
    client.print(packet);
    client.stop();
  }  
  
}

void cbk(int packetSize) {
  packet = "";
  packSize = String(packetSize,DEC);
  for (int i = 0; i < packetSize; i++) { packet += (char) LoRa.read(); }
  LoRaData(packetSize);
}

void setup() { 

  //Serial.begin(115200);

   //WIFI Kit series V1 not support Vext control
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.Heltec.Heltec.LoRa Disable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
 
  Heltec.display->init();
  Heltec.display->flipScreenVertically();  
  Heltec.display->setFont(ArialMT_Plain_10);
  Heltec.display->clear();

  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.disconnect(true);
  delay(1000);
  WiFi.mode(WIFI_STA);
  WiFi.setAutoConnect(true);
  WiFi.begin("Linksys_Lolo","LoLa2013");
  delay(100);


  //Initialize the I2C sensors and ping them
  if (!sensor.begin()) {
    Heltec.display->clear();
    Heltec.display->drawString(0, 0, "Did not find Si7021 sensor!");
    Heltec.display->display();
    delay(1000);
    while (true);
  }

  byte count = 0;
  while(WiFi.status() != WL_CONNECTED && count < 10)
  {
    count ++;
    delay(500);
    Heltec.display -> drawString(0, 0, "Connecting...");
    Heltec.display -> display();
  }

  Heltec.display -> clear();
  if(WiFi.status() == WL_CONNECTED)
  {
    Heltec.display -> drawString(0, 0, "Connecting...OK.");
    Heltec.display -> display();
//    delay(500);
  }
  else
  {
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Connecting...Failed");
    Heltec.display -> display();
    while(1);
  }
  Heltec.display -> drawString(0, 10, "WIFI Setup done");
  Heltec.display->drawString(0, 20, "Wait for incoming data...");
  Heltec.display->display();
  delay(1000);
    
  LoRa.receive();
}

void loop() {
  int packetSize = LoRa.parsePacket();
  if (packetSize) { 
    cbk(packetSize);  
  } else {
    retry++;
    if (retry > MAX_RETRY) {
       temp_out = NULL_VALUE;
       humidity_out = NULL_VALUE;
       retry = MAX_RETRY;
    }
  }
  stateLoop++;
  if (stateLoop > CHANGE_STATE) 
  {
     stateLoop = 0;
     state++;
     temp = sensor.readTemperature();
     humidity = sensor.readHumidity();
     switch (state) {
        case 1:
           displayTemp(temp,"Int");
           break;
        case 2:
           displayTemp(temp_out,"Ext");
           break;
        case 3:
           displayData(temp,humidity,temp_out, humidity_out);
           break;          
        case 4:
           displayPA(pa);
           break;
        case 5:
           displayUV(mV);
     }
     if (state >= 5) {
        state = 0;     
     }
  }  
  delay(500);
}
